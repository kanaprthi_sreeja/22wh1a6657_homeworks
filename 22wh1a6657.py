# -*- coding: utf-8 -*-
"""22WH1A6657.ipynb

Automatically generated by Colaboratory.

Original file is located at
    https://colab.research.google.com/drive/1axq_PfzgPLW8cDfhJBCZwzhN8Nw4DERL
"""

#assignment
#1
#use map to create a new list by changing each country to upper case in the country list
list1=['india','australia','korea','america','china']
def upper_c(str1):
  return str1.upper()
result=list(map(upper_c,list1))
print(result)

#2
#use filter to filter out countries having exactly six chars
list1=['india','russia','america','brazil','newzealand']
def six_char(str1):
    if len(str1)==6:
      return str1
result=list(filter(six_char,list1))
print(result)

#3
#create a function returning a dictionary,where keys stand for starting letters of countries and values are the number of country names starting with that letter
def num_of_countries(list1):
  country_dict={}
  for country in list1:
    first_letter=country[0].lower()
    if first_letter in country_dict:
      country_dict[first_letter]+=1
    else:
      country_dict[first_letter]=1
  return country_dict
strings=['india','korea','japan','china','canada','russia']
start_count=num_of_countries(strings)
print(start_count)

#4
#For all numbers 1-10000, use a nested list/dictionary comprehension to find how many numbers are divisible by each single digit 3-9 and print the single digit which has highest number of multiples
def highest_multiples_digit():
  highest_divisible={}
  for digit in range(1,10):
    highest=0
    for num in range(1,1001):
      if num%digit==0:
        highest=max(highest,num)
    highest_divisible[digit]=highest
  return highest_divisible
print(highest_multiples_digit())